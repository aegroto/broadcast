import logging

from modules.broadcast.BroadcastManager import BroadcastManager

def initialize():
    logging.info("Initializing broadcast module...")

    BroadcastManager.initialize()

def connect():
    pass

def load_manager():
    return BroadcastManager.load()

def depends_on():
    return ["bot"]