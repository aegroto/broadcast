import telegram, logging, os, traceback, time

from datetime import datetime

from modules.pytg.ModulesLoader import ModulesLoader

def broadcast_job(context):
    bot = context.bot

    logging.info("Running broadcast job...")

    # Loading data
    data_manager = ModulesLoader.load_manager("data")
    broadcast_data = data_manager.load_data("broadcast", "broadcast")

    broadcast_finished = False

    # Define send function based on media type
    if broadcast_data["media_type"] == "none":
        def send_func(chat_id):
            bot.sendMessage(
                chat_id = chat_id,
                text = broadcast_data["text"],
                parse_mode = telegram.ParseMode.MARKDOWN
            )
    elif broadcast_data["media_type"] == "image":
        def send_func(chat_id):
            bot.sendPhoto(
                chat_id = chat_id,
                caption = broadcast_data["text"],
                photo = broadcast_data["media_id"],
                parse_mode = telegram.ParseMode.MARKDOWN
            )
    elif broadcast_data["media_type"] == "video":
        def send_func(chat_id):
            bot.sendVideo(
                chat_id = chat_id,
                caption = broadcast_data["text"],
                video = broadcast_data["media_id"],
                parse_mode = telegram.ParseMode.MARKDOWN
            )
    elif broadcast_data["media_type"] == "gif":
        def send_func(chat_id):
            bot.sendAnimation(
                chat_id = chat_id,
                caption = broadcast_data["text"],
                animation = broadcast_data["media_id"],
                parse_mode = telegram.ParseMode.MARKDOWN
            )
    
    # Send the message to as much recipients as possible
    garbage = []

    for user_chat_id in broadcast_data["remaining_recipients"]:
        try:
            logging.info("Sending message to {}".format(user_chat_id))

            send_func(user_chat_id)

            garbage.append(user_chat_id)

            if len(broadcast_data["remaining_recipients"]) - len(garbage) <= 0:
                broadcast_finished = True
                break

            if len(garbage) >= 10:
                logging.info("Rush finished")
                break
        except telegram.error.RetryAfter as e:
            logging.info("Exception {} while sending, breaking the round".format(e))

            break
        except (telegram.error.BadRequest, telegram.error.Unauthorized) as e:
            logging.info("Exception {} while sending, removing the id".format(e))

            garbage.append(user_chat_id)

            if len(broadcast_data["remaining_recipients"]) - len(garbage) <= 0:
                broadcast_finished = True
                break
        except:
            logging.info("Unhandled exception during broadcast")

            traceback.print_exc()

            return

        time.sleep(0.1)

    for garbage_user_id in garbage:
        broadcast_data["remaining_recipients"].remove(garbage_user_id)
    
    # Save updated data
    data_manager.save_data("broadcast", "broadcast", broadcast_data)

    # Schedule next round
    if not broadcast_finished:
        context.job_queue.run_once(broadcast_job, when=2)
