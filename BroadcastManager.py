import yaml, os, logging

from modules.pytg.Manager import Manager
from modules.pytg.ModulesLoader import ModulesLoader

from modules.broadcast.handlers.jobs.broadcast import broadcast_job

class BroadcastManager(Manager):
    @staticmethod
    def initialize():
        BroadcastManager.__instance = BroadcastManager()

        return

    @staticmethod
    def load():
        return BroadcastManager.__instance

    def schedule_broadcast(self):
        logging.info("Scheduling broadcast...")
        bot_manager = ModulesLoader.load_manager("bot")

        bot_manager.updater.job_queue.run_once(broadcast_job, when=0)

        return